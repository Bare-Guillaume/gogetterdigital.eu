<?php get_header();
// --- Container 1 variables --- //
$container_1 = get_field('container-1');
$container_1_main_tag_line = $container_1['main-tag-line'];
$container_1_sub_tag_line = $container_1['sub-tag-line'];
$container_1_banner_mobile = $container_1['banner-mobile'];
$container_1_banner_medium = $container_1['banner-medium'];
$container_1_content_heading = $container_1['content-heading'];
$container_1_content_sub_heading = $container_1['content-sub-heading'];
$container_1_content_text = $container_1['content-text'];
// --- Container 2 variables --- //
$container_2 = get_field('container-2');
$container_2_content_heading = $container_2['heading'];
$container_2_video = $container_2['video'];
$container_2_content = $container_2['content'];
// --- Container 3 variables --- //
$container_3 = get_field('container-3');
$container_3_content = $container_3['content'];
// --- Container 4 variables --- //
$container_4 = get_field('container-4');
$container_4_heading = $container_4['heading'];
$container_4_content = $container_4['content'];
// --- Container 5 variables --- //
$container_5 = get_field('container-5');
$container_5_heading = $container_5['heading'];
$container_5_cards = $container_5['cards'];
$container_5_card_1 = $container_5_cards['card-1'];
$container_5_card_1_image = $container_5_card_1['image'];
$container_5_card_1_heading = $container_5_card_1['heading'];
$container_5_card_1_content = $container_5_card_1['content'];
$container_5_card_1_button = $container_5_card_1['button'];
$container_5_card_2 = $container_5_cards['card-2'];
$container_5_card_2_image = $container_5_card_2['image'];
$container_5_card_2_heading = $container_5_card_2['heading'];
$container_5_card_2_content = $container_5_card_2['content'];
$container_5_card_2_button = $container_5_card_2['button'];
// --- Container 6 variables --- //
$container_6 = get_field('container-6');
$container_6_heading = $container_6['heading'];
$paragraph_1 = $container_6['paragraph-1'];
$paragraph_2 = $container_6['paragraph-2'];
$paragraph_3 = $container_6['paragraph-3'];
$paragraph_4 = $container_6['paragraph-4'];
$paragraph_5 = $container_6['paragraph-5'];
$paragraph_6 = $container_6['paragraph-6'];
$paragraph_7 = $container_6['paragraph-7'];
$paragraph_8 = $container_6['paragraph-8'];
$paragraph_9 = $container_6['paragraph-9'];
$container_6_image = $container_6['image'];
$container_6_conclusion = $container_6['conclusion'];
// --- Container 7 variables --- //
$container_7 = get_field('container-7');
$container_7_heading = $container_7['heading'];
$container_7_cards = $container_7['cards'];
$container_7_card_1 = $container_7_cards['card-1'];
$container_7_card_1_image = $container_7_card_1['image'];
$container_7_card_1_heading = $container_7_card_1['heading'];
$container_7_card_1_content = $container_7_card_1['content'];
$container_7_card_1_button = $container_7_card_1['button'];
$container_7_card_2 = $container_7_cards['card-2'];
$container_7_card_2_image = $container_7_card_2['image'];
$container_7_card_2_heading = $container_7_card_2['heading'];
$container_7_card_2_content = $container_7_card_2['content'];
$container_7_card_2_button = $container_7_card_2['button'];
$container_7_conclusion = $container_7['conclusion'];
// --- Container 8 variables --- //
$container_8 = get_field('container-8');
$container_8_heading = $container_8['heading'];
$container_8_content = $container_8['content'];
$container_8_button = $container_8['button'];


?>
<section class="d-md-none">
    <div id="#first-container" class="container">
        <div class="row">
            <div class="col">
                <h1 class="text-uppercase"><?= $container_1_main_tag_line ?></h1>
                <p class="text-uppercase uni-sans-thin subheader"><?= $container_1_sub_tag_line ?></p>
            </div>
        </div>
    </div>
    <div class="container intro d-block">
        <div class="row">
            <div class="col px-0">
                <img src="<?= $container_1_banner_mobile['url']; ?>" class="img-fluid" alt="Responsive image">
            </div>
        </div>
    </div>
    <div class="container container-border-left">
        <div class="row">
            <div class="col">
                <h2><?= $container_1_content_heading ?></h2>
                <p><?= $container_1_content_sub_heading ?></p>
                <p><?= $container_1_content_text ?></p>
            </div>
        </div>
    </div>
</section>
<!-- Display on medium devices start -->
<style>
    .Hero_shot_ipad {
        background-image: url("<?= $container_1_banner_medium['url']; ?>")
    }
</style>
<section class="d-none d-md-block d-lg-none">
    <div class="jumbotron jumbotron-fluid bg-white Hero_shot_ipad">
        <div class="row pl-4">
            <div class="col-6">
                <h1 class="text-uppercase"><?= $container_1_main_tag_line ?></h1>
                <p class="text-uppercase uni-sans-thin subheader"><?= $container_1_sub_tag_line ?></p>
            </div>
        </div>
        <div class="row row-border-left">
            <div class="col-5">
                <h2><?= $container_1_content_heading ?></h2>
                <p><?= $container_1_content_sub_heading ?></p>
                <p><?= $container_1_content_text ?></p>
            </div>
        </div>
    </div>

</section>
<!-- Display on medium devices end -->
<!-- Display on large devices start -->
<style>
    .Hero_shot_desktop {
        background-image: url("<?= $container_1_banner_medium['url']; ?>")
    }
</style>
<section class="d-none d-md-none d-lg-block">
    <div class="jumbotron jumbotron-fluid bg-white Hero_shot_desktop">
        <div class="row pl-4 h-50">
            <div class="col-6">
                <h1 id="Main-title" class="text-uppercase"><?= $container_1_main_tag_line ?></h1>
                <p id="Main-subtitle" class="text-uppercase uni-sans-thin subheader"><?= $container_1_sub_tag_line ?></p>
            </div>
        </div>
        <div class="row h-50 row-border-left">
            <div class="col-4">
                <h2 id="Main-definition-title"><?= $container_1_content_heading ?></h2>
                <p id="Main-definition-subtitle"><?= $container_1_content_sub_heading ?></p>
                <p id="Main-definition-text"><?= $container_1_content_text ?></p>
            </div>
        </div>
    </div>
</section>
<!-- Display on large devices end -->
<div class="container py-3 d-md-none ">
    <div class="row justify-content-center">
        <div class="col-12 text-center">
            <p class="uni-sans video-title">
                <?= $container_2_content_heading ?>
            </p>
            <div class="embed-responsive embed-responsive-16by9 frame-video mx-auto">
                <iframe class="embed-responsive-item" src="<?= $container_2_video ?>?rel=0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <div class="row py-3 mt-3 container-dark">
        <div class="col">
            <p class="uni-sans">
                <?= $container_2_content ?>
            </p>
        </div>
    </div>
</div>

<!-- Display on medium devices start-->
<div class="container-fluid d-none d-md-block d-lg-none bg-primary text-white pt-3 pb-5">
    <div class="row  justify-content-center py-4">
        <p class="uni-sans video-title"><?= $container_2_content_heading ?></p>
    </div>
    <div class="row justify-content-center">
        <div class="col-5 text-center">
            <iframe class="embed-responsive-item" src="<?= $container_2_video ?>?rel=0" allowfullscreen></iframe>
        </div>
        <div class="col-5">
            <p class="uni-sans"><?= $container_2_content ?></p>
        </div>
    </div>
</div>
<!-- Display on medium devices end-->

<!-- Display on large devices start-->
<div id="container-2" class="container-fluid  d-none d-md-none d-lg-block bg-primary text-white py-5 px-5">
    <div class="row  justify-content-center  pb-3">
        <p id="container-2-title" class="uni-sans video-title"><?= $container_2_content_heading ?></p>
    </div>
    <div class="row row-video-desktop-main  ">
        <div class="col-6  ">

            <iframe id="container-2-video" class="embed-responsive-item" width="100%" height="100%" src="<?= $container_2_video ?>?rel=0" allowfullscreen></iframe>

        </div>
        <div class="col-6">
            <p id="container-2-text" class="uni-sans"><?= $container_2_content ?></p>
        </div>
    </div>
</div>
<!-- Display on medium large end-->
<div class="container-fluid  px-md-5 pt-3">
    <div class="row">
        <div class="col">
            <p id="container-3-text" class="uni-sans">
                <?= $container_3_content ?>
            </p>
        </div>
    </div>
</div>
<div id="container-4" class="container-fluid  container-dark px-md-5 py-5">
    <div class="row">
        <div class="col">
            <h3 id="container-4-title" class="uni-sans-bold"><?= $container_4_heading ?></h3>
            <p  id="container-4-text" class="uni-sans"><?= $container_4_content ?></p>
        </div>
    </div>
</div>
<div id="container-5" class="container-fluid  px-md-5 py-3">
    <div class="row text-center">
        <div class="col-12">
            <h3 id="container-5-title" class="uni-sans-bold pb-3 text-primary"><?= $container_5_heading ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 mb-3">
            <div class="card mx-auto border-0">
                <img class="card-img-top shadow" src="<?= $container_5_card_1_image['url'] ?>" alt="Card image cap">
                <div class="card-body px-0">
                    <h4 class="card-title text-primary text-center uni-sans-bold"><?= $container_5_card_1_heading ?></h4>
                    <p class="card-text uni-sans-thin text-primary"><?= $container_5_card_1_content ?></p>
                    <a href="<?= $container_5_card_1_button['link']; ?>" class="btn btn-block btn-secondary btn-left shadow text-white uni-sans-bold"><?= $container_5_card_1_button['text']; ?></a>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 mb-3">
            <div class="card mx-auto border-0">
                <img class="card-img-top shadow" src="<?= $container_5_card_2_image['url'] ?>" alt="Card image cap">
                <div class="card-body px-0">
                    <h4 class="card-title text-primary text-center uni-sans-bold"><?= $container_5_card_2_heading ?></h4>
                    <p class="card-text uni-sans-thin text-primary"><?= $container_5_card_2_content ?></p>
                    <a href="<?= $container_5_card_2_button['link']; ?>" class="btn btn-block btn-secondary btn-right shadow text-white uni-sans-bold"><?= $container_5_card_2_button['text']; ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="container-6" class="container-fluid  px-md-5 py-5 bg-primary">
    <div class="row justify-content-center">
        <div class="row">
            <h3 id="container-6-title" class="text-secondary"><?= $container_6_heading ?></h3>
        </div>
        <div class="row px-3">
            <div class="col-12">
                <h5 class="text-secondary"><?= $paragraph_1['heading']; ?></h5>
                <p class="text-white uni-sans"><?= $paragraph_1['content']; ?></p>
            </div>
            <div class="col-12">
                <h5 class="text-secondary"><?= $paragraph_2['heading']; ?></h5>
                <p class="text-white uni-sans"><?= $paragraph_2['content']; ?></p>
            </div>
            <div class="col-12">
                <h5 class="text-secondary"><?= $paragraph_3['heading']; ?></h5>
                <p class="text-white uni-sans"><?= $paragraph_3['content']; ?></p>
            </div>
            <div class="col-12">
                <h5 class="text-secondary"><?= $paragraph_4['heading']; ?></h5>
                <p class="text-white uni-sans"><?= $paragraph_4['content']; ?></p>
            </div>
            <div class="col-12">
                <h5 class="text-secondary"><?= $paragraph_5['heading']; ?></h5>
                <p class="text-white uni-sans"><?= $paragraph_5['content']; ?></p>
            </div>
            <div class="col-12">
                <h5 class="text-secondary"><?= $paragraph_6['heading']; ?></h5>
                <p class="text-white uni-sans"><?= $paragraph_6['content']; ?></p>
            </div>
            <div class="col-12">
                <h5 class="text-secondary"><?= $paragraph_7['heading']; ?></h5>
                <p class="text-white uni-sans"><?= $paragraph_7['content']; ?></p>
            </div>
            <div class="col-12">
                <h5 class="text-secondary"><?= $paragraph_8['heading']; ?></h5>
                <p class="text-white uni-sans"><?= $paragraph_8['content']; ?></p>
            </div>
            <div class="col-12">
                <h5 class="text-secondary"><?= $paragraph_9['heading']; ?></h5>
                <p class="text-white uni-sans"><?= $paragraph_9['content']; ?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 ">
            <img id="container-6-img" src="<?= $container_6_image['url'] ?>" class="img-fluid pb-3 mx-auto d-block" alt="Responsive image">
            <p class="text-white uni-sans"><?= $container_6_conclusion ?></p>
        </div>
    </div>
</div>
<div id="container-7" class="container-fluid  px-md-5 py-5">
    <div class="row text-center">
        <div class="col-12">
            <h3 id="container-7-title" class="uni-sans-bold pb-3 text-primary"><?= $container_7_heading ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 mb-3 ">
            <div class="card mx-auto border-0 h-100 ">
                <img class="card-img-top shadow" src="<?= $container_7_card_1_image['url'] ?>" alt="Card image cap">
                <div class="card-body d-flex flex-column px-0">
                    <h4 class="card-title text-primary text-center uni-sans-bold"><?= $container_7_card_1_heading ?></h4>
                    <p class="card-text uni-sans-thin text-primary"><?= $container_7_card_1_content ?></p>
                    <a href="<?= $container_7_card_1_button['link']; ?>" class="btn btn-block btn-left mt-auto btn-secondary shadow text-white uni-sans-bold"><?= $container_7_card_1_button['text']; ?></a>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 mb-3 ">
            <div class="card mx-auto border-0 h-100">
                <img class="card-img-top shadow" src="<?= $container_7_card_2_image['url'] ?>" alt="Card image cap">
                <div class="card-body d-flex flex-column px-0">
                    <h4 class="card-title text-primary text-center uni-sans-bold"><?= $container_7_card_2_heading ?></h4>
                    <p class="card-text uni-sans-thin text-primary"><?= $container_7_card_2_content ?></p>
                    <a href="<?= $container_7_card_2_button['link']; ?>" class="btn btn-block btn-right mt-auto btn-secondary shadow text-white uni-sans-bold"><?= $container_7_card_2_button['text']; ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p id="container-7-conclusion" class="uni-sans"><?= $container_7_conclusion ?></p>
        </div>
    </div>
</div>
<div id="container-8" class="container-fluid  px-md-5">
    <div class="row d-block text-center">
        <h6 id="container-8-title"><?= $container_8_heading ?></h6>
    </div>
    <div class="row">
        <div class="col-12">
            <p id="container-8-text" class="uni-sans"><?= $container_8_content ?></p>
            <div class="row text-center">
                <div class="col-12">
                    <a id="container-8-cta" href="<?= $container_8_button['link']; ?>" class="btn d-block d-md-inline-block btn-secondary text-white uni-sans-bold shadow"><?= $container_8_button['text']; ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>