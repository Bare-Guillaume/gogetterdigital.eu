// Add your custom JS here.

(function($) {

    var wideScreen = window.matchMedia("(min-width: 992px)");

    $(document).ready(function($) {
        if (wideScreen.matches) {
            desktopAnimations();
        }

    });

    function desktopAnimations() {

        var tl_container_1 = gsap.timeline({
            scrollTrigger: {
                trigger: "#main-title",
                start: "Top bottom",
                toggleActions: "play none none complete"
                    // https://greensock.com/docs/v3/Plugins/ScrollTrigger/ // 
            }
        });

        tl_container_1.from("#Main-title", {
            duration: 2,
            text: ""
        });

        tl_container_1.from("#Main-subtitle", {
            opacity: 0,
            xPercent: -100,
            duration: 1
        });

        tl_container_1.from("#Main-definition-title", {
            opacity: 0,
            xPercent: -100, //Gsap property to move te item 100% of its width.//
            duration: 1
        });
        tl_container_1.from("#Main-definition-subtitle", {
            opacity: 0,
            y: 100,
            duration: 1
        });
        tl_container_1.from('#Main-definition-text', {
            opacity: 0,
            scale: 0,
            ease: "back"
        });

        var tl_container_2 = gsap.timeline({
            scrollTrigger: {
                trigger: "#container-2", // Trigger animation when element is in viewport //
                start: "top center",
                toggleActions: "play none none complete"
            }
        });

        tl_container_2.from("#container-2-title", {
            opacity: 0
        });

        tl_container_2.from("#container-2-text", {
            opacity: 0,
            xPercent: 100,
            duration: 0.5
        });

        tl_container_2.from("#container-2-video", {
            opacity: 0,
            xPercent: -100,
            duration: 0.5
        });

        gsap.from('#container-3-text', {
            scrollTrigger: {
                trigger: "#container-3-text",
                toggleActions: "play none none complete",
                start: "top center",
                // markers: true
            },
            opacity: 0,
            scale: 0,
            duration: 0.5,
            ease: Linear.easeNone
        });

        var tl_container_4 = gsap.timeline({
            scrollTrigger: {
                trigger: "#container-4",
                start: "top center",
                toggleActions: "play none none complete"
            }
        });

        tl_container_4.from("#container-4-title", {
            duration: 1.5,
            text: ""
        });

        tl_container_4.from("#container-4-text", {
            opacity: 0,
            scale: 0,
            duration: 0.5,
            ease: Expo.easeOut
        });

        var tl_container_5 = gsap.timeline({
            scrollTrigger: {
                trigger: "#container-5",
                start: "Top center",
                toggleActions: "play none none complete"
            }
        });

        tl_container_5.from("#container-5-title", {
            duration: 1.5,
            text: ""
        });

        tl_container_5.from("#container-5 .row .col-12 .card img", {
            duration: 0.5,
            scale: 0
        });

        tl_container_5.from("#container-5 .row .col-12 .card .card-body .card-title", {
            duration: 1.5,
            text: ""
        });

        tl_container_5.from("#container-5 .row .col-12 .card .card-body .card-text", {
            opacity: 0,
            scale: 0,
            ease: "back"
        });

        tl_container_5.from("#container-5 .row .col-12 .card .card-body .btn-left", {
            opacity: 0,
            xPercent: -100,
            duration: 1
        });

        tl_container_5.from("#container-5 .row .col-12 .card .card-body .btn-right", {
            opacity: 0,
            xPercent: 100,
            duration: 1
        }, "<"); // Gsap position parameter, used here to play btn-left and right //
        // animation at the same time //
        // https://greensock.com/position-parameter //

        var tl_container_6 = gsap.timeline({
            scrollTrigger: {
                trigger: "#container-6",
                start: "Top center",
                toggleActions: "play none none complete"
            }
        });

        tl_container_6.from("#container-6-title", {
            duration: 1.5,
            text: ""
        });

        tl_container_6.from('#container-6 .row .row .col-12 h5', {
            opacity: 0,
            duration: 0.5
        });

        tl_container_6.from('#container-6 .row .row .col-12 p', {
            opacity: 0,
            duration: 1
        });

        tl_container_6.from('#container-6-img', {
            opacity: 0,
            scale: 0,
            rotation: 1440
        });

        var tl_container_7 = gsap.timeline({
            scrollTrigger: {
                trigger: "#container-7",
                start: "Top center",
                toggleActions: "play none none complete"
            }
        });

        tl_container_7.from("#container-7-title", {
            duration: 1.5,
            text: ""
        });

        tl_container_7.from("#container-7 .row .col-12 .card img", {
            duration: 0.5,
            scale: 0
        });

        tl_container_7.from("#container-7 .row .col-12 .card .card-body .card-title", {
            duration: 1.5,
            text: ""
        });

        tl_container_7.from("#container-7 .row .col-12 .card .card-body .card-text", {
            opacity: 0,
            scale: 0,
            ease: "back"
        });

        tl_container_7.from("#container-7 .row .col-12 .card .card-body .btn-left", {
            opacity: 0,
            xPercent: -100,
            duration: 1
        });

        tl_container_7.from("#container-7 .row .col-12 .card .card-body .btn-right", {
            opacity: 0,
            xPercent: 100,
            duration: 1
        }, "<");

        tl_container_7.from("#container-7-conclusion", {
            opacity: 0,
            scale: 0
        });

        var tl_container_8 = gsap.timeline({
            scrollTrigger: {
                trigger: "#container-8",
                start: "Top bottom",
                toggleActions: "play none none complete"
            }
        });

        tl_container_8.from("#container-8-title", {
            duration: 1.5,
            text: ""
        });

        tl_container_8.from("#container-8-text", {
            opacity: 0,
            duration: 1
        });

        tl_container_8.from("#container-8-cta", {
            duration: 2.5,
            scale: 0,
            ease: "bounce.out",
            y: -800
        });
    }
})(jQuery);